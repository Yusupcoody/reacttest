// import logo from './logo.svg';
import './App.css';
import {  BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Navbar from './component/Navbar';
import Sidebar from './component/Sidebar';
import Home from './pages/Home'

function App() {
  return (
    <Router>
      <Navbar/>
      <div className='container'>
        <Sidebar/>
          <Routes>
            <Route path="/"  element={<Home />}></Route>

          </Routes>
      </div>
    </Router>
  );
}

export default App;
